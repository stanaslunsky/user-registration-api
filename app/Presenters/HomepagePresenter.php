<?php

declare(strict_types=1);

namespace App\Presenters;

use App\db\DatabaseInitializer;
use Nette;
use Nette\Application\UI\Form;


final class HomepagePresenter extends Nette\Application\UI\Presenter
{
    /** @var DatabaseInitializer @inject * */
    public DatabaseInitializer $databaseInitializer;

    public function beforeRender()
    {
        parent::beforeRender();

        if(DB_INIT) {
            $this->databaseInitializer->dbInit();

            exit('Tabulky byly úspěšně načteny! Vypněte načítání inicializačního skriptu');
        }
    }

    /**
     * Create registration form
     *
     * @return Form
     */
    protected function createComponentRegistrationForm(): Nette\Application\UI\Form
    {
        $form = new Nette\Application\UI\Form;
        $form->addText('fname', 'Jméno:')
            ->setHtmlId('fname')
            ->setHtmlAttribute('class', 'form-control')
            ->setRequired();
        $form->addText('lname', 'Příjmení:')
            ->setHtmlId('lname')
            ->setHtmlAttribute('class', 'form-control')
            ->setRequired();
        $form->addPassword('password', 'Heslo:')
            ->setHtmlId('password')
            ->setHtmlAttribute('class', 'form-control')
            ->setRequired();
        $form->addEmail('email', 'E-mail:')
            ->setHtmlId('email')
            ->setHtmlAttribute('class', 'form-control')
            ->setRequired();
        $form->addSelect('permission', 'Oprávnění:', [1 => 'Administrátor', 2 => 'Uživatel'])
            ->setHtmlId('permisson')
            ->setPrompt('Zvolte oprávnění')
            ->setHtmlAttribute('class', 'form-select')
            ->setRequired();
        $form->addSubmit('send', 'Registrovat uživatele')
            ->setHtmlId('send')
            ->setHtmlAttribute('class', 'form-control btn btn-primary');

        if (V_TEST)
            $form->setDefaults([
                'fname' => 'Stanislav',
                'lname' => 'Slunský',
                'email' => 'test@test.cz',
                'permission' => 1
            ]);

        $form->onSuccess[] = [$this, 'registrationFormSucceeded'];

        return $form;
    }

    /**
     * Registration form succeeded
     *
     * @param Form $form Form
     * @param array $data User's data
     * @throws Nette\Application\AbortException
     */
    public function registrationFormSucceeded(Form $form, array $data): void
    {
        $url = "http://localhost/user-registration-api/www/api-user/register-user";
        $stringData = http_build_query($data);

        $httpHeader = array(
            'Content-Type: application/x-www-form-urlencoded',
            'Content-Length: ' . strlen($stringData)
        );

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,$httpHeader);
        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        if ($response === true) {
            $this->flashMessage('Uživatel byl úspěšně zaregistrován');
        } else {
            $this->flashMessage('Během registrace došlo k chybě');
        }

        $this->redirect('Homepage:');
    }
}
