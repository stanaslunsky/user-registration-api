<?php

declare(strict_types=1);

namespace App\Presenters;

use Nette;
use App\Model\ApiUserModel;
use Nette\Application\Responses\JsonResponse;


class ApiUserPresenter extends Nette\Application\UI\Presenter
{
    /** @var ApiUserModel @inject * */
    public ApiUserModel $apiUserModel;

    /**
     * API register user
     *
     * @return bool User registered successfully
     * @throws Nette\Application\AbortException
     */
    public function actionRegisterUser(): bool
    {
        $httpRequest = $this->getHttpRequest();
        $post = $httpRequest->getPost();

        $result = $this->apiUserModel->createUser($post);

        $this->sendResponse(new JsonResponse(($result)));
    }

    /**
     * API get all registered users
     *
     * @return array False or users data
     * @throws Nette\Application\AbortException
     */
    public function actionGetAllUsers(): array
    {
        $result = $this->apiUserModel->getAllUsers();

        if ($result === false)
            $this->sendResponse(new JsonResponse([]));

        $this->sendResponse(new JsonResponse($result));
    }
}