<?php

namespace App\db;


use Nette\Database\Explorer;

class DatabaseInitializer
{
    private Explorer $database;

    /**
     * DatabaseInitializer constructor.
     * @param Explorer $database
     */
    public function __construct(Explorer $database)
    {
        $this->database = $database;
    }

    /**
     * @return bool
     */
    public function dbInit(): bool
    {
        $sql = file_get_contents('files/db_init.txt');
        $result = $this->database->query($sql);

        return true;
    }
}