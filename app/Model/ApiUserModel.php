<?php

namespace App\Model;

use Nette\Database\Explorer;
use Nette\Security\Passwords;


class ApiUserModel
{
    private Explorer $database;

    /**
     * ApiUserModel constructor.
     * @param Explorer $database
     */
    public function __construct(Explorer $database)
    {
        $this->database = $database;
    }

    /**
     * Create new user
     *
     * @param array $data User's data
     * @return bool User was succesfully created
     */
    public function createUser(array $data): bool
    {
        if (empty($data)) {
            return false;
        }

        $password = new Passwords(PASSWORD_BCRYPT);

        $result = $this->database
            ->table('user')
            ->insert([
                'fname' => $data['fname'],
                'lname' => $data['lname'],
                'email' => $data['email'],
                'password' => $password->hash($data['password']),
                'permission_id' => $data['permission']
            ]);

        if ($result === false) {
            return false;
        }

        return true;
    }

    /**
     * Load all registered users
     *
     * @return array Users
     */
    public function getAllUsers(): array
    {
        $users = $this->database
            ->table('user')
            ->select('user.id, user.fname, user.lname, user.email, permission.name')
            ->joinWhere('permission', 'permission.id = user.permission_id')
            ->fetchAssoc('id');

        return $users;
    }
}