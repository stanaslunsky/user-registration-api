# User Registration API

Simple application with API for user registration and existing users selection with register form as an example.

## Installation 
1. Choose you're folder and run command: ```git clone https://gitlab.com/stanaslunsky/user-registration-api.git```
2. In project folder run command: ```composer update``` (check composer website for installation)
3. Create database ```user_registration```
4. Application in default uses constant ```DB_INIT``` in ```Bootstrap.php``` for database initialization. Set constant as ```true``` for the first time and open homepage (```http://localhost/user-registration-api/www/```) and all tables and data will be loaded. After that set constant as ```false``` again.
5. Now you can register new users.

### Get existing users
1. Visit https://reqbin.com/
2. Set URL to http://localhost/user-registration-api/www/api-user/get-all-users
3. Set method to POST
4. Send button

## Required
- Apache (for example xampp)
  - PHP 7.4
  - cURL extesion
  - MySQL database